---
title: "[How-To] Firewall- uwf unter Ubuntu 22.04"
date: 2023-02-03T21:09:20+01:00
draft: false
author: Alexander
tags: ["ubuntu", "linux", "firewall", "bash"]
categories: ["faq", "tools"]

---

Die Standard-Firewall unter Ubuntu 22.04(Jammy Jellyfish) ist `ufw` und ist eine Abkürzung für "uncomplicated firewall". `ufw` ist ein sogenanntes Frontend für die Linux-typischen `iptables`-Befehle,  es wurde aber so entwickelt, dass grundlegende Firewall-Aufgaben von jedem User auch ohne Kenntnisse von `iptables` durchgeführt werden können.

`ufw` sollte nach der Installation von Ubuntu 22.04-System bereits vorhanden sein und muss nicht extra installiert werden. Es könnte jedoch sein, dass Firewall im Status "inaktiv" ist, und `ufw` somit ausgeschaltet ist. 

In diesem Beitrag habe ich ein Paar grundlegende Befehle zusammengefasst, die zeigen sollen, wie man mit `ufw` klar kommt.


Fogendes Befehl prüft den aktuellen Status des Firewalls

``` bash
$ sudo ufw status
Status: Inaktiv
```

So kann die Firewall aktiviert oder deaktiviert werden

``` bash
$ sudo ufw enable
Die Firewall ist beim System-Start aktiv und aktiviert


$ sudo ufw disable
Die Firewall ist beim System-Start inaktiv und deaktiviert

```

Mit der Option `verbose` bei der Statusabfrage gibt es eine Ausgabe mit deutlich mehr Informationen. Unteranderem werden die offenen Ports angezeigt.
```bash
$ sudo ufw status verbose
Status: Aktiv
Protokollierung: on (low)
Voreinstellung: deny (eingehend), allow (abgehend), deny (gesendet)
Neue Profile: skip

Zu                         Aktion      Von
--                         ------      ---
22/tcp                     ALLOW IN    Anywhere
80/tcp                     ALLOW IN    Anywhere
22/tcp (v6)                ALLOW IN    Anywhere (v6)
80/tcp (v6)                ALLOW IN    Anywhere (v6)

```

Um ein Port zu öffnen, wird die Option `allow` mit der Angabe des Ports und des Protokols verwendet.
```bash
sudo ufw allow 80/tcp
Regelk aktualisiert
```