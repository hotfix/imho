---
title: "Nobodies - Nach Dem Tod Komplettlösung"
date: 2022-11-05T11:11:48+02:00
draft: false
author: "Alexander"
tags: ["Games",  "Android",  "Puzzel",  "Spiele",  "Komplettlösung"]
categories: ["Games"]
---

Nach dem mich bereits der ersten Teil "Nobodies-Tatortreiniger"  begeistert hat. Bin ich vor einer Woche durch Zufall auf den zweiten Teil im Playstore gestossen.  Die Begeisterung war groß, da ich das Spiel mit zum Teil recht kniffligen Aufgaben wirklich gut finde.  Eine [Komplettlösung](https://hotfix.gitlab.io/imho/2020/05/30/nobodies-tatortreiniger-komplettl%C3%B6sung/) zu dem ersten Teil findet man in einem meiner früheren Beiträge.

In diesem Beitrag findest du die Komplettlösung zu dem zweiten Teil des Spiels (Nobodies - Nach Dem Tod).
Wie ich die Einzelnen Kapitel durchgespielt habe, kann man sich in meine [YT Kanal](https://www.youtube.com/watch?v=5dtHKGta9kk&list=PL6Tij_07_x6lWnBbJpSVKjjsgwj3LR3ba) ansehen. Sollte der eine oder andere Schritt in diesem Beitrag nicht kalr sein, dann wird es bestimmt aus dem dazugehörigen Video deutlich :)
Viel Erfolg beim Spielen!


## Liste der Missionen

- [Liste der Missionen](#liste-der-missionen)
  - [Mission I](#mission-i)
  - [Mission II](#mission-ii)
  - [Mission III](#mission-iii)
  - [Mission IV](#mission-iv)
  - [Mission V](#mission-v)
  - [Mission VI](#mission-vi)
  - [Mission VII](#mission-vii)
  - [Mission VIII](#mission-viii)
  - [Mission IX](#mission-ix)
  - [Mission X](#mission-x)


---

### Mission I
Die erste Mission unter dem Namen "Bohrkopf" ist ein kurze Tutoral Mission. Hier werden eingetlich nur kurz die Grundlagen anhand einer echten Mission erläutert.

Als erstes sammelt man das Brecheisen und den Betonsteintein und die Leiche ein. Jetzt öffnet man die mit dem Brecheisen das Fass und legt die Leiche rein. Jetzt nur noch Fass aufsammeln und im Inventar den Betonsteintein in das Fass mit Leiche legen. Zum Schluss Fass links ins Wasser werfen und Brechstangen zurück auf ihren Platz ablegen. Fertig!


---

### Mission II 
In der Mission "Morast" startet mal in einem Haus wo die Leiche mitten im Raum liegt. Hier hat man nun nicht nur einen Raum, sondern man hat auch die Möglichkeit aus dem Haus raus zu gehen. Lösung ist aber noch recht einfach:

- Schnapp dir die Angelrute
- Nimm dir den Eimer mit dem Wischmop
- gehe raus( unten auf den Pfeil klicken)
- Nimm die Schubkarre
- gehe wieder rein
- Benutze die Schubkarre um die Leiche einzusammeln
- Wische das Blut mit dem Mop auf
- gehe wieder raus
- Wirf die Leiche ins Wasser, damit die von Krokodilen aufgefressen wird.
- Lege auch gleich die Schubkarre zurück
- Nimm die Angel und hole die Kleidungsstücke aus dem Wasser
- Reinige den Wischmop im Wasser
- gehe wieder rein
- lege Eimer und Angelrute wieder zurück
- Verbrenne die Kleidungsstücke im Kamin

---

### Mission III

Die Mission "Roadkill" beginnt vor einem Cafe. Vor dir steht ein LKW, wo sich die Leiche befindet.

- Cafe betreten
- die Dame hinterm Tresen ansprechen und Bier bestellen
- Bier auf den Boden werfen
- Jetzt das Geld aus Dose auf dem Tresen nehmen
- Daneben Mikrofon einschalten
- Mit dem Geld die Musikeinschalten (rechts neben dem Tresen steht die Musikbox)
- jetzt ein mal raus und wieder rein gehen, die Dame steht wieder hinter dem Tresen und der LKW Fahrer sitzt auch da
- mit dem Geld Pizza bei der Dame bestellen
- wieder raus gehen und nach links gehen. Da ist eine Baustelle
- mit dem Arbeiter schnacken oder einfach gleich Pizza anbieten
- Baustelle verlassen 
- LKW Schlüssel aus der offenen Kabine holen
- Laderaum aufschliessen und Leiche holen
- jetzt auch gleich laderaum abschliessen, mit dem Schlüssel!
- Schlüssel wieder zurück in die Kabine legen
- mit der Leiche zur Baustelle gehen
- Schaufel nehmen und damit ein Lock buddeln
- Leiche reinlegen
- Lock wieder zubuddeln
- Schaufel zurückstellen
- wieder ins Cafe gehen und Geld ind die Dose legen
- Fertig!


---

### Mission IV
Du befindest dich am Eingang zu einem Friedhof. Deine Mission heisst "Totengräber" und du musst die Leiche auf dem Friedhof verschwinden lassen.

- Karte links auf dem Tisch nehmen. Jetzt habt ihr die möglichkeit im oberen Bereich die Karte anzuschauen und zu einzelnen Punkten direkt zu springen.
- Grüne Mülltonne mitnehmen
- Karte öffnen und zu Nummer 3 springen, wo man die Leiche findet
- Leiche erst mal nicht einsammeln, **nur** den Teppich mitnehmen!
- Jetz wieder Karte öffnen und zu Nummer 1 gehen.
- Teppich auf den Boden vor dem Gärtner ablegen
- Schlüssel nehmen und damit die Tür aufschliessen
- rein gehen und Kombizange sowie die Wasserpumpenzange nehmen.
- jetzt an der Wand auf den Kasten klicken
- mit der Kombizange die ersten beiden Kabel durchtrennen
- jetzt auf die gleichen Kabel klicken, dann werden diese überkreuz verbunden
- Karte öffnen und zu C gehen.
- hier mit der Wasserpumpenzange kaputten Zaun einsammeln
- Karte öffnen 3 gehen
- Leiche einsammeln und im Inventar in der Mülltonne verstecken
- Karte öffnen und zu B gehen, hier steht ein Sarg
- mit dem Stück vom Zaun den Sarg öffnen
- Im inventar auf die Mülltonne klicken und Leiche auswählen
- Leiche im Sarg ablegen und Sarg schliessen.
- Jetzt wieder zu C gehen und Stück vom Zaun wieder zurücklegen
- Dann zu 1 Springen und das Häuschen betreten
- jetzt an der Wand auf den Kasten klicken 
- mit der Kombizange wie Dräte wieder zurück verbinden
- beide Zangen wieder im Häuschen zurück legen und raus gehen
- Häuschen abschliessen und Schlüsel zurück zu der Giesskanne
- Teppich aufnehmen
- Jetzt zu 3 Springen und hier Teppich zurück legen
- Wieder zum Eingang und die Karte mit der Mülltonne zurücklegen
- Fertig!


---

### Mission V
Dies ist die Mission "Blinder Passagier" wo man die Leiche in einem Koffer verstecken muss und den Koffer dann mit einem der Fluggäste unterjubelt. Der Ausgangspunkt ist vor dem Eingang in das Flughafenterminal.

- Gepäckwagen nehmen
- ins Terminal gehen
- zum Check in gehen
- Mitarbeiterausweis nehmen
- reinigungsalkohol aus dem Verbankskasten nehmen
- Gepäckwagen zu den anderen abstellen und Münze nehmen
- Zurück zu dem vorherigen Raum
- mit der Münze Kaffee holen
- Hier kann man die anwesenden befragen, um herauszufinden welcher Fahrgast der richtige ist.
Es wird die Frau mit dem Roten Koffer sein.
- jetzt zu der Gepäckausgabe gehen und auf den roten Koffer klicken.
- Name und FLugnummer merken
- Wieder zum check-in gehen und Kaffee auf dem Tresen verschüten. Mitarbeiter ist jetzt weg!
- Check-in mit den Daten durchführen und Boardkarte vom Besitzer des roten Koffers holen.
- mit der Boardkarte zur Gepäckausgabe gehen
- roten Koffer abholen
- Im inventar mit dem alkohol die Aufkleber vom Koffer lösen
- jetzt wieder nach draussen gehen und Leiche nehmen
- Leiche im Koffer verstecken
- Boardkarte in der grünen Tonne entsorgen und reingehen
- roten Koffer gegen den roten austauschen
- jetzt wieder zu der Gepäckausgabe und Koffer an Mitarbeiter abgeben
- zurück zum chek-in und Ausweis und Alkohol zurück legen.
- Verbankskasten schliessen!
- fertig


---

### Mission VI
In der Mission "Kanarienvogel" startest du auf einem Parkplatz in der Nähe von einem Strand und einer Baustelle.

- Arbeitskleidung aus dem Auto nehmen
- zum Strand gehen
- Toten fisch nehmen und hinter dem Zelt ablegen. Pärchen ist dann weg
- Kamera nehmen
- auf dem Dach vom Häuschen die Vogelspikes nehmen
- jetzt zur Baustelle gehen
- unter dem Stapler die Vogelspikes ablegen. Fahrer ist dann weg
- Wagenheber nehmen
- Zange nehmen
- Baukontainer betreten
- mit der Cam Foto von der Karte machen
- wieder nach draussen gehen und Wagenheber unter den Baukontainer platzieren
- jetzt in den Bergwerk gehen. Auf der Karte kann man die die Punkte sehen, wo man hin fahren kann und wo es was spannendes gibt.
- zu Punkt Eins fahren:  Links-Links-Rechts
- Hammer nehmen
- Auf der Karte wieder auf den Eingang klicken
- jetzt wieder in das Bergwerk und zu zwei fahren. Rechts-Mitte-Mitte-Rechts-Links-Rechts
- Seil aufnehmen
- und noch mla zum eingang und wieder rein
- jetzt müssen wir zu einem Ausgang fahren: Rechts-Mitte-Links-Rechts-Links-Rechts-Mitte
- Hier Seil am Stein befestigen und durch die öffnung Bergwerk verlassen. Man landet  am Strand.
- Zurück zu dem Ort am Strand mit dem Häuschen.
- Mit dem Hammer Häuschen aufbrechen und Sauerstoffflasche nehmen
- Cam kann wieder zurück in die Tasche vom Pärchen.
- Zum auto gehen und Leiche aus dem Kofferraum nehmen.
- Wieder zum Strand und dann zu dem Eingang ins Bergwerg, da wo man mit dem Seil runtergekommen ist
- Leiche an Seil befestigen, ins Bergwerg steigen und Leiche reinholen
- ACHTUNG bevor man Leiche aufnimmt, muss man den 4. Weg in dem Bergwerk gefahren sein, sonst kann man nicht direkt hin springen.
- Auf der Karte wieder auf den Eingang klicken
- jetzt wieder in das Bergwerk und zum Letzten Ziel fahren. Links-Mitte-Rechts-Rechts
- zu der Leiche gehen und aufnehmen
- jetzt wieder zu dem abgesperrtem Bereich  zurück und die Leiche reinlegen
- Saustoffflasche dazulegen und mit dem Hammer drauf hauen
- Hammer im Bergwerk am 01 Ablegen
- Seil am 02 Ablegen
- Zange vor dem Baukontainer
- Vogelspikes nehmen
- Wagenheber wieder zurück unter den Stapler
- Vogelspikes zurück aufs Dach am Strand und Häuschen schliessen.
- Kleidung zurück ins Auto, Kofferraum schliessen
- Karte auf dem Parkplatz in die Tonne werfen.
- Fertig


---

### Mission VII
In der "Vulkankrater" Mission startet am Ufer eines Flusses in der nähe des National Parks. 

- Zum Eingang gehen
- Nummernschild einsammeln
- weiter in den Park rein
- Grüne Tonne öffnen und leere Flasche entnehmen
- Machete nehmen
- Karte nehmen
- Miskgabel nehmen
- KArte öffnen und zum Pfad gehen
- Mit Machete Seil abschneiden
- Bohnenhülsen nehmen
- Steine nehmen
- Karte öffnen und zum Campingplatz gehen
- Im Inventar Bohnenhülsen in die leere Flasche legen
- Flasche mit Bohnenhülsen neben der Gruppe platzieren. Schlangen geräusch verjagt die!
- Messer nehmen
- jetzt zurück zu den Toiletten
- mit dem Messer das Rad vom Rasenmähen abschrauben
- jetzt zur der Leiche, da wo man gestartet ist
- mit Machete Babus hacken
- Im Inventar Babus mit Seil zu Floß verbinden
- Floß aufs wasser legen
- Leiche einsammeln und aufs Floß legen
- Jetzt auf Floss 2x klicken, diese sollte sich umdrehen und dann in der ferne verschwinden
- Zurück zum Campingplatz, hier sollte das FLoss nun stehen
- Nummernschild mit Mistgabel verbinden
- Kajak nehmen und auf Floss klicken
- Wagen nehmen und im Invetar mit dem Rad verbinden.
- Jetzt Leiche in Kajak und Kajak auf den Wagen legen
- Rucksack nehmen
- und zum Geysir C gehen
- Steine ins Rucksack 
- auf Kaja klicken und leiche mit Rucksack ausstatten
- Leiche ins Wasser werfen
- Zurück zum Campingplatz
- Kajak, Wagen ohne Rad und Messer zurück legen
- jetzt zum Pfad und den Seil zurück auf sein Platz
- nun zu den Toiletten und mistgabell, Machete, das Rad die Karte zurück legen
- die Flasche in die Tonne werfen und diese dann schliessen
- zurück zum Eingang und Nummernschild ablegen
- Am Ufer nur noch Babus zurück auf sein Platz stecken.
- Fertig




---

### Mission VIII
Mitternacht ist eine recht umfangreiche Mission. Man in der Villa und muss die Leiche, welche im OG in einem abgeschlossenem Raum ist sauber zu entsorgen.

- Mistelzweig über der Tür nehmen
- Rechte Tür nehmen und in den Keller gehen
- Gasmaske, Stricknadel und Garnknäuel nehmen und wieder  zurück
- Jetzt die Küche betreten, Mittlere Tür
- Dosenöffner nehmen und  weiter durch die Tür zum Garten
- Hier Holz und Schaufel nehmen und zurück zum Eingang
- jetzt die Treppe hoch
- Mistelzweig mit Garnknäuel verbinden und am Geländer befestigen 
- Diplom von der Wand nehmen und im Iventar Dosenöffner benutzen
- Dimplom unter die Tür schieben
- Mit Stricknadel den Schlüssel  durch schieben und  Diplom wieder einsammeln
- Zimmer Öffnen und Schlüssel aus der Schüssel nehmen
- Zürück in den Garten und mit dem neuen SCHlüssel Häuschen öffnen
- Fläschchen  nehmen
- Zurück zum Eingang und jetzt nach links gehen
- Steichhölzer nehmen
- jetzt in den Keller gehen
- Holz ins Offen legen und Anzünden
- Heizung in der Küche auf max stellen(alles ausser 3. von links ausstellen)
- In die Küche gehen und Tortenheber aufnehmen
- Zum Eingang und die Uhr mit dem Tortenheber öffnen und Uhrzeit auf 12(mitternacht) stellen
- Jetzt zu party und Fläschchen an der Nebelmaschine benutzen
- Jetzt zur Leiche und diese aufnehmen
- Mit der Leiche in die Küche gehen
- die Leiche in der Eismaschine einfrieren
- Im Invetar die gefrorene Leiche mit der Schaufel zerkleinern und in der Spüle entsorgen
- Dosenöffner und Tortenheber zurück auf ihre Plätze in der Küche legen und in den Garten gehen
- Fläschchen im Häuschen abstellen und dieses mit dem Schlüssel abschliessen
- Schaufel zurück legen
- in dem Partyraum die Steichhölzer zurücklegen
- Im OG, Schlüssen zurück in die Schüssel, Tür mit dem Schlüssel abschliessen und Schlüssel unter die Tür Schieben
- Diplom wieder an die Wand hängen
- Mistelzweig einsammel
- Am Eingang, Mistelzweig zurück an die Tür, Uhr abschliessen
- Im Keller Gasmaske, Stricknadel und Garnknäuel zurück legen und Offen ausmachen
- Fertig

---

### Mission IX
Toter Briefkasten heist die folgende Mission, in der man in einem Appartment startet. Hier liegt auch bereits die Leiche, welche man los werden muss.

- Fläschen, Tickets, Seil und Kleiderbügel aus dem Schrank und den Schraubenschlüssel aus der Schublade nehmen
- raus gehen
- mit Kleiderbügel das Auto aufschliessen und Rollstuhl nehmen
- weiter nach links gehen
- Seil und Schraubenschlüssel an die Luftballons binden 
- Telefon anklicken und Kaugummi nehmen
- Gebäude betreten
- Mit Tickets Zugang verschaffen
- Wasserflasche nehmen
- Schlüsselfach Nr 14 öffnen und Sonnenbrille entnehmen
- In den Aufzug und ganz nach oben fahren
- Im Invetar Wasserflasche mit dem Abführmittel verbinden und die Flasche zu den anderen ablegen
- Mit Kaugummi Münzschlitz in dem Fernsichtgerät verkleben
- nach hin und her sollte im Fernsichtgerät eine Münze vorhanden sein
- wieder in den Aufzug und runter fahren. 
- Im Aufzug Rufnummer  und Nammen für Wartung merken
- Gebäude velassen und am Telefon die Nummer anrufen und den Namen nennen.
- Man bekommen den den Code 9364
- Jetzt kann man zu der Leiche gehen, diese einsammeln, im Inventar auf den Rollstuhl setzen und mit der Sonnenbrille maskieren
- Wieder zurück ins Gebäude
- Im Aufzug auf Wartung drücken und den Code eingeben
- Hier Mitarbeiteranzug nehmen
- Leiter hoch
- Schraubenschlüssel nehmen und Seilzug demolieren
- Schraubenschlüssel wieder zurück an die Luftbalons
- jetzt wieder zurück, Anzug wieder aufhängen und zu der Aussichtplattform
- Leiche in den Gurt und los schicken. -> Leiche Stürtz ab
- jetzt wieder runter 
- Brille ins Fach legen und abschliesssen
- Gebäude Verlassen
- Seil und Schraubenschlüssel bei den Luftbalons einsammeln und zurück zum Auto
- Rollstuhl rein und mit dem Kleiderbügel abschliessen
- Ins Haus und hier die restlichen Gegenstände zurücklegen.
- Fertig



---

### Mission X 
Die Letzte Mission "Wunderland" spielt in einem Vergnügungspark ab.

- Gewichte aufnehmen
- Zu der Mülltonne gehen und Zange aufnehmen
- jetzt zurück und zwei mal nach rechts gehen
- hier mit dem Mickey wegen Anstecker sprechen
- Jetzt in den Abgesperrten Bereich gehen und Handschuhe und die Tube nehmen
- jetzt zurück zu dem Häuschen, wo die Leiche liegt
- Im Inventar mit der Zange den Anstecker zerlegen => Sicherheitsnadel
- Mit der Nadel das Schloss aufschliessen. Einfach ausprobieren und Reihnfolge finden
- Im Häuschen Eimer, Ölkanister und Sack mit Lauge nehmen
- Raus und Eimer mit Wasser füllen
- Jetzt wieder zu dem Bereich , wo der Mickey steht und da in den abgesperrten Bereich gehen
- Mit der Zange das Faß mit dem Öl aufmachen und Ölkanister füllen
-  jetzt wieder ein schritt zurück 
- mit dem Öl die vorder Kugel schmieren und mit den Handschuhen abnehmen
- Zurück zu dem Häuschen mit der Leiche
- Die Kugel mit der Farbe färben
 - und los zu dem Wurfstand
 - Mit der eigenen Kugel werfen und geschenk abholen. Kugel wieder einsammeln
 - weiter zum Mickey und die Kotze beim Mitarbeiter platzieren. Alle sind jetzt weg.
 - Zurück zur Kanone und Gewichte einsammeln.
 - jetzt zur Leiche und leiche einsammeln
 - Jetzt ins Häuschen und Gewicht der Leiche bestimmen mit den Gewichten.
 - Alles einsammeln und wieder zu Kanoe gehen.
 - Hier die Leiche in die Kanone stecken und Gewicht auf 75kg einstellen. Abschuss!
 - jetzt noch Gewichte wieder zurück legen, Die Abdeckung bei der Kanone wieder zurücklegen
 - Zum Häuschen und Kugel abwaschen
 - zum Abgesperrten Bereich gehen.
 - Faß wieder schliessen und mit Zange festmachen
 - Handschuhe und Kleber zurück legen
 - Leiche sollte da sein. Einsammeln
 - Jetzt ins Gruselhaus gehen
 - Wasser und Lauge in den Kessel geben
 - Leiche in den Kessel stecken
 - wieder zudecken und rausgehen
 - Kugel zurück aus Stange
 - Zum Häuschen und vor dem Häuschen die Zange ablegen
 - in dem Häuschen den Rest
 


Bei Fragen oder Anregungen einfach kommentieren :)
 Viel Spass!