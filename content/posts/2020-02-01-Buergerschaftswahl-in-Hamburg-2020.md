---
title: "Buergerschaftswahl in Hamburg 2020"
date: 2020-02-01T15:27:24+01:00
draft: false
categories: ["life"]
tags: ["hamburg", "wahlen"]
author: "Alex"
---

Ich habe gerade im [wahl-o-mat](https://www.wahl-o-mat.de/hamburg2020/) die aktuellen Themen zu der Bürgerschaftswahlen 2020 in Hamburg angeschaut. 

Diese drei Fragen sind mir ganz besonders aufgefallen:

![wahl2020-1](/img/20200201_wahl2020_1.PNG)

![wahl2020-2](/img/20200201_wahl2020_2.PNG)

![wahl2020-3](/img/20200201_wahl2020_3.PNG)

Wenn wird uns heute mit solchen Themen politisch auseinander setzen müssen, dann geht es uns eigentlich allen ganz gut.