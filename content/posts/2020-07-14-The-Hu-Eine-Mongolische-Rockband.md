---
title: "The Hu - Eine mongolische Rockband"
date: 2020-07-14T22:29:23+02:00
draft: false
author: Alex
tags: ["Rockband", "Musik", "Hunnu Rock"]
categories: ["Music"]
---

Ich höre eigentlich  gerne Musik, aber kann mich schon gar nicht mehr erinnern, wann ich das letzte Mal auf Youtube Musikvideos angeschaut habe. Als ich heute Youtube aufgemacht habe, war auf der Startseite ein Musikvideo von der Band namens [The Hu](https://de.wikipedia.org/wiki/The_Hu) mit einem witzigen Titel “Yuve Yuve Yu”. 

Der Song ist ja sowas von geil. IMHO. Wer Rockmusik mag und die Band nicht kennt, sollte unbedingt einen Blick drauf werfen.

Die Band “The Hu” ist relativ neu und kommt aus der Mongolei. Sie spielen auf den traditionellen mongolischen Musikinstrumenten und nennen ihren Musikstill *Hunnu Rock*.

Hier ist der Link zu dem  offiziellen Kanal der Band auf Youtube: https://www.youtube.com/channel/UCs6vRDdkZ8bP8Xt6WHbvrwA



