---
title: "Online Tutor Für Python"
date: 2023-02-02T10:31:08+01:00
draft: false
author: Alexander
tags: ["python", "tutor"]
categories: ["dev", "tools"]

---

Durch Zufall bin ich auf ein recht interessantes Tool für Python Anfänger, das sogenannte Online Tutor, gestossen.Es ermöglicht, die Funktionsweise von Python-Programmen besser zu verstehen, in dem es grafisch dargestellt wird, wie sich Stack und Heap bei jedem Programmschritt verändern.

Mit dieser visuellen Darstellung kann man die Ausführung von Programmcode besser verstehen und die Funktionsweise bestimmter Konstrukte und Algorithmen besser nachvollziehen. Man kann damit auch die Fehler im eigenen  Code erkennen und beheben.

Der Online-Python-Tutor kann auch dazu beitragen, die Konzepte der Datenstrukturen und Algorithmen besser zu verstehen.

Ich denke besonders für Anfänger kann dieses Tool sehr nützlich sein. 

Die Anwendung ist recht simpel. Man kann entweder seinen eigenen Code in das Editor-Fenster kopieren oder die bereits vorgegebene einfache Beispiele, wie die Fakultät, die Quadratwurzel oder die Fibonacci-Folge, anschauen. Mit den Pfeiltasten auf der Tastatur lässt sich einfach durch den Code navigieren und die Funktionsweise des Codes besser verstehen.

Nachteil ist, dass dieVersion von Python nicht ständig aktualisiert wird.

Den Online-Tutor findet man unter: https://pythontutor.com/visualize.html#mode=edit