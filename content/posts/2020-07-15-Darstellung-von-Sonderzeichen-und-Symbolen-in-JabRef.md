---
title: "Darstellung von Sonderzeichen und Symbolen in JabRef"
date: 2020-07-14T23:06:48+02:00
draft: false
author: Alex
tags: ["Literaturverwaltung", "JabRef", "Sonderzeichen", "Symbole", "Umlaute", "Latex"]
categories: ["Software", "Tools"]
---

Für die Literaturverwaltung beim Schreiben von Texten benutze ich gerne [JabRef](https://www.jabref.org/). Die Software ist kostenlos und lässt sich sehr einfach bedienen. 

Da ich die Texte, wenn ich mal welche schreiben muss, gerne in [Latex](https://de.wikipedia.org/wiki/LaTeX) oder auch [Lyx](https://www.lyx.org/) schreibe, speichere ich meine Literaturliste immer im Bibtex Format ab. Nicht oft, aber es kommt schon mal vor, dass die Namen von Quellen oder auch von Autoren Sonderzeichen oder auch Symbole enthalten. Diese müssen bei der Angabe entweder Maskiert oder durch Latex-Befehle ersetzt werden.

Aus diesem Grund habe ich hier eine kleine Liste zusammen gestellt, wie die einzelnen Zeichen und Symbole ersetzt werden müssen. 



| Symbol/ Sonderzeichen | Latex     |
| :--: | :--: |
|  &copy;   |   ` {\textcopyrigh}`   |
| &trade; | `{\texttrademark}` |
| &reg; | `{\textregistered}` |
| &amp; | `\&` |
| ö / Ö | `\"o`  `\"O` |
| ä / Ä | `\"a` `\"A` |
| ü / Ü | `\"u` `\"U` |
| ß | `\ss` |

