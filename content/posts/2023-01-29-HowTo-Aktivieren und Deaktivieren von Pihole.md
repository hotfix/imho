---
title: "[HowTo] Aktivieren Und Deaktivieren Von Pihole"
date: 2023-01-29T19:07:56+01:00
draft: false
author: Alexander
tags: ["pihole", "api", "befehle"]
categories: ["faq", "dev", "tools"]

---

Es gib unterschiedliche Möglichkeiten um pihole zu aktivieren oder zu deaktivieren.

## API

Pi Hole verfügt über eine API, welche über den Aufruf im Browser, PostMan oder Konsole verwendet werden kann.
Dies kann sehr nützlich sein, wenn man pihole in eigenen Projekten einbinden möchte und z.b. die Anwendung mit Sprachbefehlen steuern möchte oder bestimmte Prozesse automatisieren möchte.

Um Pihole aktivieren oder deaktivieren zu können, können folgende Endpunkte der API verwendet werden:

+ URL deaktivieren: http://[Pihole Adresse]/admin/api.php?disable&auth=[WEB_PASSWORD]
+ URL aktivieren: http://[Pihole Adresse]/admin/api.php?enable&auth=[WEB_PASSWORT]
+ Deaktivieren für `[X]` Sekunden: http://[Pihole Adresse]/admin/api.php?disable=`[X]`&auth=[WEB_PASSWORD]


[WEB_PASSWORD] ist nicht das Passwort, welches man für den Webzugriff verwendet, sondern es ist eine 64-stellige Zeichenkette.
Dieses Passwort findet man auf dem Pihole Server in der Daten /etc/pihole/setupVars.conf.


API-Dokumentation: https://discourse.pi-hole.net/t/pi-hole-api/1863


## Konsole

Es besteht die Möglichkeit pihole auch direkt aus der Konsole zu steuern:
Um pihole zu aktivieren kann folgendes Befehl verwendet werden
`> pihole enable`

und zum deaktivieren `> pihole disable`

weitere Befehle findet man unter folgendem Link: https://docs.pi-hole.net/core/pihole-command/