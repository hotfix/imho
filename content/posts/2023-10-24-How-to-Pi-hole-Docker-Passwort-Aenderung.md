---
title: "[How-To] Pi-Hole Docker Passwort Änderung"
date: 2023-10-24T20:06:47+02:00
draft: false
author: Alexander
tags: ["pihole", "passwort", "docker"]
categories: ["faq", "Software", "tools"]

---
Es gibt Fälle, in den man das Passwort für die Pi-Hole Adminoberfläche ändern möchte.
Wenn Pi-Hole jedoch in einem Docker Container läuft, muss man zunächst die Konsole des Containers in dem die Pi-Hole Instanz läuft starten


``` bash
$ docker exec -it pihole /bin/bash
```

Jetzt kann man das Passwort neu vergeben.

``` bash
$ pihole -a -p neues-passwort
```

Alternativ kann man das Passwort auch komplett entfernen

``` bash
$ pihole -a -p
```

## Referenzen:
- [How do I set or reset the Web interface Password?](https://discourse.pi-hole.net/t/how-do-i-set-or-reset-the-web-interface-password/1328)
- [docker exec](https://docs.docker.com/engine/reference/commandline/exec/)