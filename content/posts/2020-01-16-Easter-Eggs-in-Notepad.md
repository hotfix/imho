---
title: "Easter Eggs in Notepad++"
date: 2020-01-16T21:47:45+01:00
draft: false
categories: ["tools"]
tags: ["notepad++", "editor", "ide"]
author: "Alex"
---

Vor einiger Zeit habe ich mir die Sourcen von [Notepad++ auf github](https://github.com/notepad-plus-plus/notepad-plus-plus) angeschaut. Durch Zufall bin ich dann auf ein lustiges Feature gestossen. Die Entwickler haben nähmlich ein Paar sogenanter Easter Eggs eingebaut.

Tippt man  z.b. `Notepad++` in dem Editor ein.  
Markiert das ganze Wort  
und wenn man jetzt  auf `F1`  drückt, dann wird ein Zitat ausgegeben. In diesem Fall:
> I hate reading other people's code.
So I wrote mine, made it as open source project, and watch others suffer.
-- Notepad++


Neben dem `Notepad++` Befehl, gibt es noch eine Menge weitere Befehle. Ein vollständige Liste mit den möglichen Befehlen und dazugehörigen Zitate kann man direkt im Code auf [Github](https://github.com/notepad-plus-plus/notepad-plus-plus/blob/v7.5.1/PowerEditor/src/Notepad_plus.cpp#L5994) finden.
