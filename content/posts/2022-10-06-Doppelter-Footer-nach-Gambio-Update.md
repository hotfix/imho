---
title: "Doppelter Footer beim Datablue-Basic Theme Nach Gambio Update"
date: 2022-10-06T11:35:25+02:00
draft: false
author: Alexander
tags: ["Gambio", "Shop", "Update", "Datablue-Basic"]
categories: ["Software"]
---

In meinem Gambio Shop benutze das Datablue-Basic Theme. Nach einem Update auf eine 4.5.x.x Version habe ich festgestellt, dass der Footer auf der Shop-Seite doppelt ausgegeben wird.

Um das Problem zu beheben muss man die `layout_footer.html` aus dem Verzeichnis `themes/Honeygrid/html/system/` nach `themes/Datablue-Basic/html/system/` kopieren.
Zum Schluss wie immer den gesamten Cache im Adminbereich `Toolbox > Cache`  von unten nach oben leeren.

Jetzt sollte auch der Footer korrekt dargestellt werden.




