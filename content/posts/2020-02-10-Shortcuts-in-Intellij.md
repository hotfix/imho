---
title: "Nützliche Shortcuts in Intellij"
date: 2020-02-11T19:47:10+01:00
draft: false
categories: ["dev", "tools"]
tags: ["java", "ide", "Intellij", "IDEA"]
author: "Alex"
---

Ich arbeite aktuell an einem Java Projekt und benutze als Entwicklungsumgebung die grossartige IDE Intellij.  

In diesem Beitrag habe ich versucht  aus meiner Sicht recht nützliche Shortcuts, die ich recht oft bei der Entwicklung  verwende, zusammenzustellen.

- `sout` - generiert `System.out.println`
- `psvm` - generiert `public static void main(String[] args)`
- `main` - generiert eine leere `main` Methode
- `prsf` - generiert `private static final `
- `psf` - generiert `public static final `
- `psfi` - generiert `public static final int `
- `psfs` - generiert `public static final string`
- `todo` -  generiert einen TODO Kommentar mit Zeitstempel :`// TODO: 10.02.2020  `
- `fixme` - generiert einen FIXME Kommentar mit Zeitstempel :`// FIXME: 10.02.2020  `

Eine Liste mit allen Shortcuts  kann mit `STRG + J` aufgerufen werden.