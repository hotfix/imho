---
title: "Nobodies Tatortreiniger Komplettlösung"
date: 2020-05-30T13:37:47+02:00
draft: false
author: Alex
tags: ["Games", "Android", "Puzzel", "Spiele", "Komplettlösung"]
categories: ["Games"]
---

Ich spiele recht selten, da mir die meisten Spiele schnell langweilig werden. Es gibt aber einige Spielegenre, die ich sehr gerne mag. Quests, Puzzlespiel und Abenteuer sind einige davon. Vor ca. einer Woche bin ich im google play store auf das Spiel [Nobodies Tatortreiniger](https://play.google.com/store/apps/details?id=com.blyts.nobodies&hl=de) gestoßen. 

In dem Spiel ist man in der Rolle eines “Cleaners”. Ziel ist es dabei nach einem Auftragsmord, die Leichen zu beseitigen und dabei keine Spuren zu hinterlassen. Klingt einfach, doch die Missionen werden immer kniffliger, vor allem, wenn man diese auch sauber und ohne Spuren beenden möchte.

Es gibt ja bereits mehr als genug Youtube Videos mit den Lösungen, aber ich poste hier trotzdem mal eine Schritt-für-Schritt Anweisung, wie man die Missionen, ohne dabei die Spuren zu hinterlassen, schafft.

*Da ich das Spiel noch nicht komplett durchgespielt habe, werde ich diesen Post von Zeit zu Zeit aktualisieren.*

## Liste der Missionen
- [Mission I: Trockenübung](#operation-i-trockenübung)
- [Mission II: Antwood](#operation-ii-antwood)
- [Mission III: Zimmerservice](#operation-iii-zimmerservice)
- [Mission IV: Gartenzaun](#operation-iv-gartenzaun)
- [Mission V: Schrottlaube](#operation-v-schrottlaube)
- [Mission VI: Polarsturm](#operation-vi-polarsturm)
- [Mission VII: Bildungsauftrag](#operation-vii-bildungsauftrag)
- [Mission VIII: Abgehoben](#operation-viii-abgehoben)
- [Mission IXI: Zaunvogel](#operation-ix-zaunvogel)
- [Mission X: Bittere Tropfen](#operation-x-bittere-tropfen)
- [Mission XI: Kuhfänger](#operation-xi-kuhfänger)

---

### Operation I: Trockenübung

- Nimm die Mauerkelle, links auf den Boden
- Nimm Sand aus der Schubkarre
- Nimm den Zementsack, hinten rechts
- Nimm die Schlüssel vom Körper
- Nimm den Körper
- Benutze den Schlüssel an dem Werkzeugkasten
- Nimm den Schraubenschlüssel aus dem Werkzeugkasten
- Benutze den Schraubenschlüssel am Wasserhahn
- Fülle den Eimer mit Wasser
- Lege Zement in den Mischer
- Lege Sand in den Mischer
- Kippe den Eimer mit Wasser in den Mixer
- Klicke auf den Mischer, um das fertige Zement zu entnehmen
- Ziehe die Mauerkelle auf das Zement (in dem Inventar)
- Lege den Körper in die Säule auf der linken Seite. Klicke erneut, um es zu komplett zu verstecken
- Benutze Zement an der Säule.
- Lege den Schraubenschlüssel zurück in das Werkzeugkasten und schließe es
- Lege die Mauerkelle zurück, links auf den Boden
- Stelle den Eimer ab neben dem Wasserhahn.

----

### Operation II: Antwood

- Nimm die Pipette auf dem Schreibtisch
- Nimm den Laborkittel 
- Verlasse den Raum
- Nimm einen Kaffee
- Nimm das gelbe Rohr rechts
- Gehe zurück in den ersten Raum
- Klicke auf den Schrank mit Chemikalien an der Wand
- Benutze die Pipette auf der Skopolamin Flasche (erste von rechts)
- Benutze die Pipette am Käfig mit Mäusen
- Klicke auf den Schrank mit Chemikalien an der Wand
- Benutze die Pipette auf der Skopolamin Flasche (erste von rechts)
- Ziehe die Pipette auf den Kaffee (in dem Inventar)
- Stelle den Kaffee in dem anderen Raum ab
- Verlasse den Raum und komme wieder zurück, die Wache schläft
- Nimm den Personalausweis auf dem Boden
- Öffne mit dem Personalausweis die Tür und betrete den nächsten Raum
- Benutzer das Rohr am Ventil 
- Drücke auf die rote Taste, um die Aquariumtür zu öffnen
- Geh zurück in den ersten Raum und Nimm den Körper
- Klicke auf den Schrank mit Chemikalien an der Wand
- Benutze die Pipette auf der Desoxyribose Flasche (zweite von links)
- Benutze die Pipette mit Desoxyribose,  um eine Ratte zu füttern und füllen die Pipette nach
- Gehe zurück in den Raum mit der Aquariumtür
- Lege den Körper in das Aquarium ( zwei mal klicken)
- Aquarium schließen, Mit Wasser wieder fühlen => am Ventil drehen
- Benutze die Pipette an dem Futterspender
- Verlasse den Raum und schließe diesen wieder ab mit der Karte
- Lege das Rohr wieder an der Tür rechts ab
- Lege die Karte wieder am Wachmann ab
- Gehe in das erste Raum
- Lege die Pipette auf dem Tisch ab
- Hänge den Laborkittel wieder auf

---

### Operation III: Zimmerservice

- Schließe die Tür und Nimm die Schilder
- Nimm das Bettgitter
- Verlasse das Zimmer 
- Hänge das "bitte nicht stören" Schild an die eigene Tür
- Hänge das "bitte reinigen" Schild and die zweite Tür
- Gehe nach oben
- Platziere Bettgitter an den Wassertanks
- Öffne den Stromkasten und Nimm die Zange
- Geh zurück in den ersten Raum
- Benutze die Zange an dem Fernseherkabel
- Geher zurück nach oben zum Stromkasten
- Benutzer den Verteiler am 5. Kabel
- Gehe wieder unter zum Reinigungswagen, der im Flur steht
- Nimm die Schlüssel, öffne die Tür auf der linken Seite und 
- Hänge die Schlüssel wieder zurück am Reinigungswagen auf
- Nimm die Bettlaken auf dem Reinigungswagen
- Gehe in den gerade geöffneten Raum
- Nimm die Sicherungen aus der Schublade
- Benutze die Zange am Wasserhahn
- Geh aufs Dach
- Benutze den Hebelgriff am Wassertankventil und  Klicke dann auf das Ventil, um Wasserversorgung zu unterbrechen
- Gehe zurück in dein Zimmer und hole den Körper und das blutige Bettlaken
- Benutze das saubere Bettlaken auf dem Bett
- Gehe in das ehemals verschlossenen Raum
- Öffne den Müllabwurfschacht, lege den Körper und das blutige Laken hinein
- Schließe den Müllabwurfschacht wieder
- Gehe mit dem Aufzug nach unten
- Benutze die Sicherungen am Schalter und dann den Schalter
- Geh wieder aufs Dach
- Öffne das Wasserventil
- Benutze die Zange am Hebelgriff
- Nimm das Bettgitter
- Öffne Stromkasten und benutze die Zange um den Verteiler zu entfernen
- Lege die Zange zurück
- Gehe in das ehemals verschlossenen Raum 
- Benutze Hebelgriff am Wasserhahn
- Verlasse den Raum und schließe die Tür
- Gehe in dein Zimmer und setzte Verteiler am Fernseher und das
- Bettgitter am Bett wieder zurück 

----

### Operation IV: Gartenzaun

- Klicke auf das Gemälde an der Wand
- Klicke auf den Safe
- Gebe 6684 als Code ein (Das ist das Datum auf dem Kühlschrank)
- Nimm die Schlüssel
- Gehe in die Küche
- Öffne Kühlschrank und nimm die Flasche 
- Öffne den Schrank unter der Spüle und nimm die Flasche mit dem Bleichmittel
- Nimm die Gummihandschuhe vom Regal
- Benutze die Schlüssel um die Tür zu öffnen
- Nimm den Zementsack
- Gehe in die Garage
- Nimm den Eimer mit der blauen Farbe
- Nimm die Zange
- Nimm den Schlüssel
- Klicke auf das Regal um es zu bewegen
- Benutze die Zange am Draht
- Gehe zurück in den ersten Raum und benutze den neuen Schlüssel um die Tür rechts aufzuschließen
- Gehe runter in den Keller
- Nehme die Spitzhacke
- Öffne den Schaltkasten
- Benutze das Kabel
- Benutze Gummihandschuhe und klicke auf das Kabel erneut
- Gehe zum Pool, benutze die leere Flasche am Pool und schalte dann die Pumpe ein
- Klicke auf das Pool, so dass die Ansicht von oben ist.
- Benutze die Spitzhacke um ein Loch zu machen
- Gehe in die Küche und nehme den Körper
- Benutze Bleichmittel um Blutspuren zu entfernen
- Stelle Bleichmittel wieder zurück unter die Spüle und mache die Tür zu
- Lege die Gummihandschuhe wieder zurück aufs Regal
- Gehe zurück zum Pool und lege den Körper ins Loch
- Vermische Wasser und Zement im Inventar
- Benutze das neue Zement um das Loch dicht zu machen
- Benutze die Farbe um die Stelle zu streichen
- Schalte die Pumpe ein
- Lege Zementsack wieder zurück
- Gehe in den Keller
- Hole das Kabel ab
- Lege die Spitzhacke zurück
- Schließe die Kellertür ab mit dem Schlüssel
- Gehe in die Garage, das Kabel wird automatisch eingesetzt
- Lege die Zange auf den Tisch
- Stelle die Farbe zurück
- Bewege das Regal wieder zurück und hänge die Schlüssel wieder auf
- Gehe in die Küche und schließe die Tür mit dem Schlüssel
- Stelle die Flasche in den Kühlschrank
- Lege die Schlüssel wieder in den Safe und schließe ihn

----

### Operation V: Schrottlaube

- Nimm den Haken im Kopf des Körpers
- Nimm die Sprühfarbe
- Nimm Holzbrett
- Verlasse die Gasse 
- Benutze den Hacken am Auto um einsteigen zu können
- Klicke auf den Sicherungskasten links neben dem Lenkrad, und deaktiviere nur die roten
- Aktiviere alle baluen und grünen Sicherungen 
- Drücke den weißen Knopf auf der Mittelkonsole(es sollte etwas passieren)
- Öfnne das Handschuhfach und nimm die Münzen
- Verlasse das Auto und benutze die Sprühfarbe am Auto
- Gehe i ndie Gasse und hole den Körper ab
- Lege den Körper in den Kofferraum und schließen Sie ihn
- Benutze die Münzen am Münztelefon
- Wähle die Nummer des Abschleppdienstes an (rechts => 555-5185)
- Lese das rote Schild
- Nimm das Kabel auf dem Boden und das Flesch aus der Mülltonne
- Verbinde Kabel mit dem Hacken im Inventar
- Benutze es an der Wand links
- Benutze das Fleisch und lege es auf der Baggerschaufel ab
- Klicke auf den Bagger und setze die Schaufel mit dem Hund hoch
- Gehe auf den Schrottplatz
- Nimm den Schraubendreher und benutze ihn um das Airbag abzumachen
- Nimm den Feuerlöscher 
- Benutze Airbag am Fenstergitter und im Anschluss den Feuerlöscher
- Steig durch das Fenster ein
- Nimm die Batterie
- Öffne die verschlossene Schublade: 6-12-9-12-3-3-12 (unten-oben-links-oben-rechts-rechts-oben)
- Nimm den Schlüssel aus der Schublade
- Gehe wieder raus und setze die Batterie in den Gabelstapler.
- Benutze den Gabelstapler
- Jetzt wird gepuzzelt: Ziel ist es das besprühte Auto an die Schrottpresse links zu bringen. Das Bild Zeigt wie die restlichen Autos stehen können, damit man das Auto mit dem Körper  zu der Schrottpresse bringen kann. Die genauen Schritte sind in dem [Video](https://www.youtube.com/watch?v=vUEKxqBX7kA) zu sehen.
![Schrottplatz](/img/20200530_nobodies_schrottplatz.png)
- Nach dem das Auto an der Presse ist, fahre nach oben
- Stecke den Schlüssel in die Zündung, Klicke auf den grünen und dann auf den roten Knopf
- Nimm den Schlüssel aus der Zündung und kehre zum Hof zurück
- Nimm die Batterie aus dem Gabelstapler heraus
- Lass die Luft aus dem Airbag mit dem Schraubendreher raus und lege beide in das rote Auto zurück
- Lege den Feuerlöscher wieder zurück 
- Steig durch das Fenster ein
- Lege die Batterie wieder neben der Tür ab
- Lege den Schlüssel in die Schublade und schließen diese 
- Gehe wieder raus und setzte das Gitter zurück auf das Fenster
- Gehe wieder nach draußen, setze die Baggerschaufel wieder runter, nimm die Schnurr und wirf sie in die Mülltonne

---


### Operation VI: Polarsturm

- Sammele die Schrotpatronen ein
- Öffne den Geräteschrank links am Haus und nimm die Spitzhacke
- Gehe ins Haus, nimm die Karte links auf dem Regal und die Machete über dem Kamin
- Setze dich ins auto. 
- Nimm die Autoschlüssel hinter der Sonnenblende 
- Nimm die Brieftasche und das Feuerzeug aus dem Handschuhfach
- Fahre zur Tankstelle: Links-Rechts-Mitte-Mitte-Links-Links;Später kann man dahin mit einem Klick auf der Karte fahren
- Nimm das Kanister
- Benutze die Machete um die auf Plane zu nehmen
- Benutze die Brieftasche an der Tanksäule
- Öffne die Karte und klicke auf das Haus
- Holfe den Körper und lege diesen auf die Ladefläche
- Decke die Ladefläche mit der Plane zu
- Benutze die Karte um zu Tankstelle zu gelangen.
- Fahre von hier aus zum Hafen: Rechts-Mitte-Links-Mitte-Rechts
- Nimm den Ancker
- Verbinde Ancker mit dem Körper im Inventar
- Benutze die Spitzhacke, um ein Lock im Eis zu machen.
- Lege den Körper in das Loch und dann auch die Schrotpatronen
- Bringe Kanister und die Plane zur Tankstelle
- Fahre zurück zum Haus
- Stelle die Spitzhacke zurück im Geräteschrank ab
- Gehe in das Haus und lege die Karte zurück auf das Regal und die Machete über dem Kamin
- Steig ins Auto und lege die Brieftasche und das Feuerzeug in das Handschuhfach und schließe es wieder.
- Nimm den Autoschlüssel, lege ihn zurück hinter die Sonnenblende und klappe die wieder ein.

---

### Operation VII: Bildungsauftrag

- Nimm die Münze aus dem Brunnen. Geh nach rechts
- Nimm den Alligatorzahn
- Sprich mit dem Jungen über den Süßwarenautomaten. (Er will dann Kaugummi gegen Schoki tauschen)
- Gehe zurück in den Raum mit dem Mamut und dann links
- Benutze den Zahn am Sicherungskasten. Schalte die Sicherung B1 aus und gehen zurück zum Eingang.
- Nimm den Ausweis vom Wachmann und gehe wieder nach links.
- Öffne die Tür und dringe den Ausweis wieder zurück
- Gehe jetzt in den so eben geöffneten Raum.
- Nimm den Speer.
- Klicken Sie auf den Safe. Gib 185679 (ASIMOV) ein. Nimm die Schlüssel mit.
- Nimm die Zutrittskarte vom Körper und den Körper. Verlasse den Raum
- Benutze die Zutrittskarte um in den Raum links reinzukommen.
- Lege den Körper auf die Liege.
- Nimm die Spritze und die Flasche mit Formalin.
- Verbinde den Schlauch von der Maschine links mit Körper. 
- Wähle die Maschine aus und dann Ausleiten.
- Nimm Kanister mit Blut und leere es in der Spüle aus
- Fülle Formalin in den Kanister im Inventar
- Gehe zurück zum Eingang und dann nach oben
- Klicke auf den Süßwarenautomaten und benutze die Münze.
- Gib D1 ein.
- Öffne mit dem Schlüssel den Raum hinten auf dem Gang und gehe hinein.
- Nimm die Uniform, Tintenpatronen, Outfit des Höhlenmenschen.
- Schau dir die Visitenkarte auf dem Tisch an. Da steht der Name McArthy. Die Karte kann wieder zurückgelegt werden.
- Gehe zurück zum Kind und tausche Schokolade gegen Kaugummi
- Geh zurück zum Körper
- Benutze Kaugummi am Körper
- Kompiniere die Spritze mit Tintenpatronen. In die Spritze muss die Farbe Y + M.(ergibt rot)
- Kombiniere die rote Spritze mit dem Kanister
- Kanister wieder an die Maschine anschließen und Einleiten drücken.
- Nimm den Körper und ziehe Outfit des Höhlenmenschen an im Inventar
- Gehe zurück nach oben, diesmalbiege in den Raum rechts.
- Ersetze einen Höhlenmenschen in der Ausstellung durch den preparirtem Körper.
- Die Frau fragt von dem der Auftrag kam, hier muss man McArthy auswählen.
- Lege die Keule in die Hand des neuen Höhlenmenschen
- Gehe zurück in den Einbalsamierungsraum
- Verschmelze den Höhlenmenschen am Dampfkessel und wirf Wachs in den Papierkorb
- Stelle die Flasche mit Formalin zurück und die Spritze auf den Tisch
- Gehe zurück in den Raum im 2. Stock.
- Stelle den Speer an die Wand, lege die Tintenpatronen auf den Tisch, hänge die Uniform auf
- Verlasse den Raum und schließe die Tür mit dem Schlüssel ab
- Geh zurück ins Büro
- Lege die Schlüssel in den Safe und schließe ihn. Verlassen den Raum
- Schliesse die beiden Türen mit der Zutrittskarte ab
- Bringe den Alligatorzahn zurück. 

---

### Operation VIII: Abgehoben

- Nimm das Messer und gehe zur Ausfahrt nach rechts
- Öffne das Tor rechts und gehe hinein
- Gehe nach links
- Nimm die Kohleschaufel und benutze sie auf dem Grill, um Kohle zu erhalten. 
- Grill schliessen und Kohleschaufel wieder abstellen
- Gehe nach links.
- Benutze die Kohle auf dem Auto und gehe dann zurück zu den Flugzeugen.
- Jetzt klicke auf die Bürotür ganz hinten
- Lese die Checkliste auf der Pinnwand und lege diese wieder zurück.
- Klicke auf die Jacke um Drogen zu erhalten. Gib sie in die Kaffeetasse und verlasse das Büro
- Sprich mit dem Mann und fragen ihn nach dem Vorhängeschloss. Die Antwort ändert sich immer. Die Zahlenkombination ist aber in den Antworten von oben nach unten.
- Verlasse das Büro und gehe nach links, wo Grill und Tischtennisplatte stehen
- Öffne das Schloss an der roten Tür 
- Nimm die Plastikfolie, den Kreuzschlüssel und den Gabelstapler
- Gehe zurück zum Körper und benutze den Kreuzschlüssel am Reifen
- Verbinde Körper mit der Plastikfolie und dann das Messer mit eigewickeltem Körper im Inventar 
- Jetzt lege den Körpe in den Reifen und gehe zurück in den Raum mit den drei Kisten.
- Benutze den Gabelstapler und nimm die drei Kisten(orange, blau und rot)
- Entferne den Körper aus dem Reifen und lege den Körper in die rote Kiste
- Gehe zu den Flugzeugen und belade diese mit den Kisten. Achte dabei auf die gleichen Farben von Kiste und Flugzeug
- Jetzt müssen die Flugzeuge betankt werden. Zu jedem Flugzeug muss erst das Erdungskabel verlegt werden und dann die Tankpistole. Danach auf Tanken drücken
- Nach dem die drei Flugzeuge getankt wurden, müssen der Erdungskabel und und der Kraftstoffschlauch wieder zurück zu dem Tankgerät
- Jetzt nur noch Plastikfolie, Kreuzschlüssel und Gabelstapler in den Laderaum zurückbringen.
- Und zum Schluss noch den Reifen zum Traktor zurückbringen

---

### Operation IX: Zaunvogel

- Gehe rechts. Nehmen Sie den Wagenheber aus dem Kofferraum, die Leiter von der Wand und die Käfig auf dem Boden. 
- Gehe ins Haus 
- Nimm die Videokassette , den Schraubenschlüssel, das Messer, Gewicht von der Angelrute, faule Äpfel und die Flasche aus dem Müll, Plastiktüte in der Nähe der Tür. Verlasse das Haus
- Öffne die Motorhaube, benutze Schraubenschlüssel an der Autobatterie und gehe nach rechts
- Nimm die Dose und benutze diese um Schlamm zu sammeln
- Gehe zweimal nach links
- Benutze den Wagenheber am Zaun
- Öffne die Sicherheitsbox und trenne das Kabel mit dem Messer durch
- Geh zurück und nimm den Wagenheber mit 
- Gehe nach links zum Eingang mit Wachposten
- Benutze das Messer am Türschloss. Benutze die Batterie für den Kurzschluss und gehe hinein
- Nimm das Feuerzeug und den Schlüssel
- Mach einen Abdruck des Schlüssels auf dem Schlamm im Inventar und lege den Schlüssel zurück
- Gehe zurück in das Haus.
- Benutze das Feuerzeug um Kamin anzuzünden. Lege das Bleigewicht um es zu schmelzen und mache danach einen Schlüssel (kombiniere das geschmolzene Blei mit dem Schlammabdruck)
- Verlasse das Haus und gehe nach rechts
- Benutze die Flasche auf dem Wasser
- Mache das Gitter auf und gehe hinein
- Nimm den Stock und kombiniere ihn mit dem Käfig zu einer Falle im Inventar 
- Stehle die Falle auf den Boden und lege den Apfel in der Falle ab. 
- Gehe raus und wieder zurück.
- Nimm die Ratte und Benutze sie
- Stelle die Leiter an die Wand unter der Gitter und benutze die Ratte auf dem Gitter.
- Benutze den Schraubenschlüssel am Gitter und gehe in den Raum hoch.
- Nimm das Buch rechts.
- Klicke auf das Monitor und benutze die Videokassette
- Drücke aud Aufnahme und dann auf Wiedergabe
- Klicke auf die Zellensteuerung und öffne Block 4, Zelle 2
- Gehen wieder runter. Nimm die Leiter. Danach gehe geraudeaus und dann die Leiter hoch
- Nimm die Farbe links und die Säure rechts. 
- Stelle die Leiter auf, um das Gitter zu erreichen. 
- Benutze Schraubenschlüssel um es zu öffnen
- Gehe zu Block 4 => Links-Mitte-Rechts-Rechts-Rechts 
- Betritt die Zelle 2
- Benutze die Säute auf der Toilette und entferne danch diese
- Nimm den Körper und lege es in das Loch
- Benutze die Farbe auf dem Loch
- Setze die Kloschüssel wieder zurück und verlasse den Raum
- Gehe durch Luftungschacht wieder zurück in den Lagerraum
- Setze das Gitter wieder zurück, stelle die Farbe ab und die Säure zurück. Nimm die Leiter und gehe wieder runter
- Benutze Schraubenschlüssel am unteren Rohrbogen, Nimm den Körper und setze Rohrbogen wiederzurück
- Gehe zurück zum roten Auto
- Setze die Plastiktütte an den Häcksler, lege den Körper rein
- Nimm die Plastiktütte und verfüttere an Schweine
- Lege Wagenheber in den Kofferraum und schließe diesen
- Lege die Batterie zurück und mache die Motorhaube zu
- Gehe zurück zum Überwachungsraum (mit der Leiter).
- Lege das buch zurück, nimm Videokassette mit und schlieese die Zelle 2. 
- Gehe wieder unter, nimm die Leiter und verlasse die Kanalisation
- Benutze den SChlüssel um Kanalisation abzuschliessen 
- Gehe zurück ins Haus
- Lege Videokassette, das Messer, den Schraubenschlüssel zurück. Lösche das Feuer mit der Flasche und leg die Flasche zurück. Gehe wieder raus
- Lege den Schlüssel in den Häcksler. Stelle die Leiter zurück

---

### Operation X: Bittere Tropfen

- Gehe geradeaus. Nimm den Deckel an dem links. 
- Gehe in die erste Tür links.
- Nimm das Klebeband aus dem Schrank und den Schraubenschlüssel neben dem Fernseher.
- Gehe zurück und dann in den zweiten Raum links 
- Nimm den Verband auf dem Tisch rechts und den Schraubendreher aus dem Schrank. Geh zurück zum Eingang.
- Benutze den Deckel auf der Spendenbox rechts 
- Gehe nach draussen
- Benutze den Schraubenschlüssel und das Klebeband auf dem Schild
- Gehe rein und dann wieder raus
- Setze das Verkehrszeichen wieder zurück mit dem Schraubenschlüssel und entferne das Klebeband.
- Gehe wieder rein und nimm die Münze aus dem Deckel und den Deckel mit
- Nimm den Schlüssel hinter dem Tresen.
- Öffne den Raum auf der linken Seite mit dem SChlüssel und gehe hinein.
- Nimm die Visitenkarten auf dem Tisch und gehe raus
- Gehe wieder geradeaus.
- Benutze die Münze an dem Snackautomaten. 
- Nimm  die Schokolade und kombiniere diese mit den Visitenkarten im Inventar
- Gehe zum Eingang und lege die Schokis auf dem Tresen ab. 
- Gehe wieder in das Büro auf der linken Seite
- Lege die Visitenkarten zurück und benutze den Computer in der rechten Ecke
- Logge dich mit 1587 ein
- Drücke 2 und gib 7475 als Patientennumme und 0200
- Drücke 1 und nimm den Ausdruck
- Gehe in das Zimmer mit zwei Patienten und benutze den Ausdruck auf dem rechten Bett
- Gehe indasZimme mit dem Körper
- Nimm den Körper und kombiniere ihn mit dem Verband.
- Brind den Körper in das andere Zimmer und lege auf dem leeren Bett ab
- Gehe zurück zum Computer.
- Drücke wieder 2 und gib 7475 3323 ein. Drücke 1 und nimm den Ausdruck.
- Drücke 3 um die Sitzung zu beenden
- Gehe wieder in das Zimme mit beiden Patienten und benutze den neuen Ausdruck auf dem rechten Bett 
- Gehe raus und jetzt gerade aus in das OP Saal. Zum Eintritt den Schraubenzieher benutzen.
- Entferne das Gitter links in der oberen Ecke mit dem Schraubenzieher und gehe in die Leichenhalle
- Öffne die rechte untere Kühlzelle, lege die Leiche hin und schliesse es 
- Verlasse die Leichenhalle, Setze das Gitter wieder zurück und verschraube es
- Verlasse OP Saal und benutze hier auch Schraubenzieher um die Tür zu verriegeln
- Bringe den Schraubenzieher zurück in den Schrank im Zimmer mit zwei Patienten
- Bringe das Klebeband und den Schraubenschlüssel zurück in das andere Zimmer
- Lege den Deckel wieder zurück
- Schließe die Bürotür ab und bring den Schlüssel zurück.

---

### Operation XI: Kuhfänger

- Nimm den Draht auf den Boden
- Rede mit der Frau über das, was sie liest.
- Gehe ein Wagen zurück und öffe das WC mit dem Draht
- Gehe rein und nimm das Klopapier
- Gehe noch ein Wagen zurück in das Damen WC
- Nimm Klopapier und lege beide in die Toilette. Klicke auf die Spühlung 
- Gehe ein Wagen nach vorne und Klicke auf das Türe wo das Licht rauskommt. Da sitzt das Personal
- Sprich mit der linken über das verstopfte Klo
- Gehe ein Wagen zurück und betritt das Abteil mit der Frau
- Nimm die Pillen und das Geld aus dem Rücksack. In der Schublade unter dem Sitz den Koffer mitnehmen und dann auch noch die Mütze auf dem Tisch
- Gehe jetzt ganz nach vorne bis in den Wagen 5.
- Stelle den Koffer gegen die Klimaanlage auf der linken Seite
- Gehe zurück in das Büro mit dem Zug Personal
- Sprich mit dem Passagier rechts. Bitte ihn Personal zu rufen wegen der Klimaanlage
- Nimm den Stift, Schlüssel aus der Schublade und das Schild "Ausser Betrieb", und die Passierliste
- Gehe in das Restaurant und kaufe zwei Vodka mit dem Geld und frage nach dem Lappen
- Vermische Pillen mit einem Vodka
- Gehe in das Frauen WC und kippe den anderen Vodka aus und fülle die Flasche mit Wasser
- Nimm das Bleichmittel mit
- Gehe in das Restaurant und trinke mit der Frau. Sie wird ohnmächtig.
- Nimm ihr Buch
- Gehe ein Wagen zurück und dann in das Abteil 33-35
- Gib dem Mann das Buch der Frau und nimm seins
- Gehe raus und manipuiere das Ticket. Im Invetar Stift und Ticket kombinieren und Wagen 2 Sitz 21 eingeben.
- Gehe wieder zu dem Mann und gib ihm das Ticket zurück
- Gehe in das Männer WC und wasche die Blutflecken mit dem Lappen. Der Lappen davor muss mit Bleichmittel kombiniert werden.
- Nimm den Körper und verlasse das WC.
- Platziere das Schild "Ausser Betrieb" an der WC Tür
- Gehe nun in das leere Abteil und plaziere den Körper auf der Bank. Klicke bis diese sich umgedreht hat und  setze die Mütze auf.
- Gehe in den Wagen 5,öffne die Tür und gehe rein.
- Nimm das Seil und öffne die nächste Tür  und gehe auch da rein
- Benutze das Wal­kie-Tal­kie. Gibt Zugnummer 8702 und Sektor 48 ein. Zug hält an.
- Gehe ein Wagen zurück und öffne die Seitentür. Gehe hinaus
- Benutze das Seil links am Hacken
- Gehe in das Abteil mit der Leiche, öffne das Fenster und binde die Leiche am seil fest.
- Gehe zurück aufs Dach und ziehe die Leiche hoch. Nimm sie und gehe nach vorne. Du landest in dem Gepäckwagen.
- Lege die Leiche in das Rohr und danach benutze das Seil. Das Rohr in die offene Tür links werfen.
- Jetzt die Tür 6 abschlissen, Seitentür abschlissen, ein Wagen zurück und die Tür5 abschliessen
- Lappen dem Barkeeper zurückgeben
- dem Personal Stift, Bleichmittel, die Passierliste zurückgeben
- Pillen, Mütze und Geld in das Rücksack zurück legen
- das Buch dem alten Mann zurückgeben
- Schlüssel aus dem Zug werfen (Fenster / Dach / offene Tür)

----
Die Lösungen findet man auch auf meinem [YouTube Kanal](https://www.youtube.com/playlist?list=PL6Tij_07_x6ms1w9NswVs-SMSaf0DQB9I).

Ich hoffe, dass es dem einen ider anderem helfen wird, wenn man mal nicht weiterkommt. Für Fragen und Anregungen bin ich offen.