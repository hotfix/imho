---
title: "Die CAMPUS-tüte"
date: 2020-11-06T21:13:39+01:00
draft: false
author: "Alex"
tags: ["studium", "goodies", "CAMPUS-tuete"]
categories: [""]

---

Ach ja diese tolle Studentenzeit und die coolen CAMPUS-Tüten, die man jeden Semester kostenlos zu Semesterbeginn an der Uni abholen konnte. Die CAMPUS-Tüte gibt mittlerweile auch Online in Ihrem [Shop](https://www.campus-tuete.de/index.html#shop). Selbstverständlich fast kostenlos, man muss nur die Versandkosten von 9,95€ übernehmen.

Normalerweise werden 2 Tüten verschickt.  In der aktuellen Corona-Zeit bekommt man für die Versandkosten drei CAMPUS-Tüten, wenn man den Artikel “*CAMPUS-Tüte Home - Corona Spezial*” kauft. Obwohl ich kein Student mehr bin, habe ich mir in dem Wintersemester bei dem Angebot zugeschlagen.  Heute sind die Tüten angekommen …

![](/img/20201105_campus1.jpg)

und folgende Goodies waren dieses Mal  in jeder Tüte dabei:

* 3 x Reese's Peanut Butter Cups
* 1 x Arcobräu Moos
* 1 x Fuze Tea
* 1 x Red Bull
* 1 x ovomaltine
* 2 x Teebeutel Meßmer Tee
* 1 x Riegel Corny Energy
* 1 x Päckchen MyEy- Pflanzliches EI

folgende Artikel waren nur in einer Tüte dabei:

* 1 x Kugelschreiber

* 1 x Fa Deo 150ml

* 1 x  PURE Hard Seltzer Refreshing Lemon-Lime

* 1 x GreenDoc Stimmungsformel (Kapseln mit L-Tryptophan)

* 1 x Packung Kaffeefilter

* 1 x Veganz Snack Bar-Peanut

  

![](/img/20201105_campus2.jpg)



Ich finde, dass der Inhalt der CAMPUS-Tüten für die ~10 Euro Versandkosten absolut in Ordnung ist. 