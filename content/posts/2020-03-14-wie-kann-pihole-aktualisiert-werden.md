---
title: "Wie kann Pihole aktualisiert werden?"
date: 2020-03-14T19:33:03+01:00
categories: ["faq","dev", "tools"]
tags: ["pihole", "security", "update"]
draft: false
author: "Alex"
---

Aktuell ist es  nicht möglich [Pihole](https://github.com/pi-hole/pi-hole) über das Webinterface zu aktualisieren. Grund dafür ist, dass bei der Aktualiesirung der Server neugestartet werden muss, und dies würde den Aktualisierungsprozess unterbrechen.

Um die Aktualisierung durchführen zu können, muss man sich auf den Rechner, wo der eigene Pihole läuft, verbinden. Am einfachsten kann man sich über SSH verbinden. Falls an dem Rechner  ein Monitor und eine Tastatur&Maus angeschlossen sind, dann kann man auch sie nutzen.

Im Anschluss kann muss man auf folgendes Befehlt ausführen:

```go
pihole -up
```

Nach ein Paar Minuten, sollte das Update auch schon durch sein.
