---
title: "OpenJDK Unter MacOS Installieren"
date: 2020-11-23T20:15:53+01:00
draft: false
author: Alexander
tags: ["java", "MacOS"]
categories: ["Software","dev", "faq"]
---

Heute habe ich mein MacBook auf die neue MacOS Version "Big Sur" geupgradet und habe mich auch entschieden Oracle JDK mit OpenJDK zu ersetzen.

Als erstes musste also die installierte JDK Version entfernt werden. Diese findet man unter `/Library/Java/JavaVirtualMachines`. Bei mir war in dem Verzeichnis ein Unterverzeichnis mit der JDK Version 1.8.0_31, welches ich dann entfernt habe.

``` zsh
cd /Library/Java/JavaVirtualMachines
sudo rm -rf jdk1.8.0_31.jdk 
```

Das OpenJDK kann von der [https://jdk.java.net/](offizielen Webseite des OpenJDK) heruntergeladen werden. Es stehen unterschiedliche Versionen zur Verfügung.

Anschließend entpackt man die *tar.gz*-Datei entweder im Terminal oder direkt im Finder. Zum entpacken in dem Terminal kann der `tar` Befehl verwendet werden. Mit den Parametern `xf` wird angegeben, dass die Dateien aus dem Archiv (in diesem Fall) in das aktuelle Verzeichnis extrahiert werden sollen. Am besten wechselt man zuerst in das Verzeichnis, wohin die *tar.gz*-Datei heruntergeladen wurde.

Am Beispiel von openjdk15 sieht das Befehl so aus:

```zsh
cd ~/Downloads
tar xf openjdk-15.0.1_osx-x64_bin.tar.gz 
```

Jetzt muss nur noch das extrahierte Verzeichnis mit dem openJdk in das Verzeichnis, wo wir gleich am Anfang das Oracle JDK entfent haben verschoben werden. Also ins: `/Library/Java/JavaVirtualMachines/`.
Man kann das Verzeichnis mit dem openJdk mit dem `mv`-Befehl verschieben oder mit dem `cp`-Befehl kopieren. Wie man es am besten mag.

```zsh
sudo mv jdk-15.0.1.jdk /Library/Java/JavaVirtualMachines/
```

Jetzt nur noch ein kleiner Test:

```zsh
java -version
javac -version
```

In ersten Fall sieht die Ausgabe so ähnlich wie hier aus:

```zsh
jdk-15.0.1.jdk -> ../Documents/jdk-15.0.1.jdk
alexander@ShusMekBuk Downloads % java -version
openjdk version "15.0.1" 2020-10-20
OpenJDK Runtime Environment (build 15.0.1+9-18)
OpenJDK 64-Bit Server VM (build 15.0.1+9-18, mixed mode, sharing)
```

und der zweite Befehl würde dann die Version des Compilers liefern: `javac 15.0.1`.

Diese Anleitung sollte auch mit älteren MacOS Versionen funktioniern, probiert habe ich es aber nicht aus.