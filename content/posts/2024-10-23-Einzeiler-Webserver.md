---
title: "Einzeiler Webserver"
date: 2024-10-23T12:07:00+02:00
draft: false
author: "Alexander"
tags: ["webserver", "php","python","node.js","ruby"]
categories: ["tools","dev"]

---


Einzeiler-Webserver sind einfache und schnell zu startende HTTP-Server, die oft mit nur einem einzigen Befehl im Terminal ausgeführt werden können. Sie sind besonders nützlich für Entwicklungs- und Testzwecke, da sie einem ermöglichen, statische Dateien oder kleine Webanwendungen schnell bereitzustellen, ohne eine vollständige Serverkonfiguration vornehmen zu müssen.

## Wofür sind Einzeiler-Webserver gut?
Einzeiler-Webserver eignen sich hervorragend für:

- Lokale Entwicklung: Schnelles Testen von Webseiten oder APIs.
- Prototyping: Einfaches Bereitstellen von Prototypen, ohne sich um komplizierte Servereinstellungen kümmern zu müssen.
- Dateifreigabe: Schnelles Teilen von Dateien innerhalb eines lokalen Netzwerks.

## Beispiele für Einzeiler-Webserver
Wenn du bereits einen Interpreter für Python, Ruby, PHP oder Node.js installiert hast, kannst du die folgenden Einzeiler verwenden:

**Python:**
```python
python -m http.server
```
(Standardmäßig auf Port 8000)

**Ruby:**
```ruby
ruby -run -e httpd . -p 8000
```

**PHP:** 
``` php
php -S localhost:8000
````

**Node.js (mit dem http-server-Paket):**

``` js
npx http-server
```