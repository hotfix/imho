---
title: "LetsEncrypt Zertifikat Unter Windows Erstellen"
date: 2021-04-05T20:13:41+02:00
draft: false
author: Alexander
tags: ["windows", "SSL"]
categories: ["faq", "Software", "tools"]

---

Meine SSL Zertifikate habe ich bis vor Kurzem über die SSL for free erstellt. Dieser Dienst fusioniert vor einiger Zeit mit dem Zero SSL, wo man immer noch kostenlos sein SSL Zertifikat erstellen kann. Es gibt aber einen Hacken, denn nach 90 Tagen können hier keine kostenlose Zertifikate mehr erstellt werden. Außerdem können bei Zero SSL keine Multidomainzertifikate kostenlos erstellt werden.  Es musste also ein anderer Weg gefunden werden.

Die [Let’s Encrypt](https://letsencrypt.org/) Zertifikate können sehr einfach unter Linux oder Mac erstellt werden. Aber auch für Windows Nutzer gibt es mehrere relativ einfache Möglichkeiten.

Die erste Möglichkeit ist die Verwendung von [Windows-Subsystem für Linux(WSL)](https://docs.microsoft.com/de-de/windows/wsl/about). Voraussetzung dafür ist, dass man bereits Windows 10 im Einsatz hat. Da dieser Weg aus meiner Sicht etwas kompliziert ist, werde ich an dieser Stelle darauf nicht weiter eingehen. 

Die einfachere Möglichkeit bietet das Tool [Crypt-LE](https://github.com/do-know/Crypt-LE). Es handelt sich um einen Kommandozeilenprogramm, welches also direkt in der Windows Kommandozeile ausgeführt werden kann.

1. Zunächst lädt man sich das Tool von der Github Seite herunter: https://github.com/do-know/Crypt-LE/releases.

2. Jetzt entpackt man das Archiv in ein Verzeichnis mit einem beliebigen Namen z.B. `c:/sslZertifikat`

3. Man öffnet ein CMD Fenster und navigiert in das in Punkt 2 erstelltes Verzeichnis
   ``` > cd c:/sslZertifikat``` 

4. Hier führt man das Crypt-LE Tools aus und folgt einfach den Anweisungen Schritt für Schritt. **Achtung**: man sollte nicht zu schnell die einzelnen Schritte überspringen.

   ``` bash
   le64 -key account.key -email "meine@domain.de" ^
     -csr domain.de.csr ^
     -crt domain.de.crt ^
     -csr-key domain.de.key ^
     -handle-as http^
     -api 2 ^
     -live ^
     -generate-missing ^
     -domains "*.domain.de"
     
   ```
   (Hier muss nur noch domain.de durch eigenen Domainnamen ersetzt werden)

5. Hat man alle Schritt erfolgreich ausgeführt, werden in dem selben Verzeichnis die Key-Dateien erzeugt.

Das war’s eigentlich. Ein neues Zertifikat kann danach sofort auf dem eigenen Server eingesetzt werden.



