---
title: "Kennwort in Remote Desktop Sitzung Ändern"
date: 2020-03-30T12:05:32+02:00
draft: false
categories: ["faq"]
tags: ["rdp", "Passwort"]
author: "Alex"
---

Seit einigen Wochen arbeite ich von Zuhause und seit einigen Tagen erinnert mich  Windows hartnäckig daran, dass ich demnächst mein Passwort ändern muss. Um dies in RDP machen zu können, muss man anstatt der guten alten Tastenkombination, `STRG`+`ALT` + `ENTF`,  eine andere verwerden. Und zwar  `STRG`+`ALT` + `ENDE`.

Soweit so gut. Doch was tut man, wenn man in der RDP Sitzung weitere RDP Sizung hat. Wie kann hier das Passwort geändert werden. Weder `STRG`+`ALT` + `ENTF`, noch die `STRG`+`ALT` + `ENDE` Tastenkombination funktionieren nicht.

Auf der [SuperUser.com](https://superuser.com/questions/519283/change-windows-password-when-in-multiple-rdp) Seite habe ich eine Lösung gefunden, auf die ich selber nicht drauf gekommen wäre.

Man braucht die Bildschirmtastatur (*"On screen keyboard"*), die mit Windows bereits mitgeliefert wird. Sucht einfach nach `osk.exe`. 

**Erster Lösungsansatz:** Versucht die Tastenkombination `STRG`+`ALT` + `ENTF` auf der Bildschirmtastatur nacheinander mit der Maus anzuklicken.

Sollte es nicht funktioniert, dann gibt es noch folgende Möglichkeit

**Zweiter Lösungsansatz:** Auf der physischen Tastatur drückt man auf die beiden Tasten `STRG`+`ALT`  und klickt dabei mit der Maus auf `ENTF` auf der Bildschirmtastatur.

Bei mir hat der zweite Ansatz funktioniert. Jetzt ist die Woche gerettet und ich muss die Hotline nicht mit dem Problem belästigen.