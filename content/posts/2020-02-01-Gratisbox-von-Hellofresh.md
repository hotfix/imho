---
title: "Gratisbox von Hellofresh oder bis zu 75% Rabatt"
date: 2020-02-01T20:41:19+01:00
draft: false
categories: ["freebe"]
tags: ["hellofresh", "gratis", "rabatt"]
author: "Alex"
---


~~**Update 15.10.2022** Aktuell gibt es wieder Gratisboxen für Neukunden.~~

~~**Update 14.03.2020** Aktuell gibt es 3 Gratisboxen für Neukunden.~~

~~**Update 03.02.2020**: Bis zum 28.02, gibt es 30 Euro Rabatt auf die erste Box. Bei Einer Box für 2 mit 3 Gerichten sind es 75% Rabatt~~

~~**Update 02.02.2020**: Alle Gratisboxen wurden vergeben.  Im Moment nur noch 20 Euro Gutscheine (50% Rabatt auf die erste Box)~~

Seit einiger Zeit bin ich ein zufriedener Kunde von [Hellofresh](https://www.hellofresh.de/) und aus diesem Grund möchte ich auch anderen die Möglichkeit geben, diesen wirklich tollen Service auszuprobieren.

![Hello Fresh- Gratisbox](/img/20200201_hellofresh_gb.PNG)

Von Zeit zu Zeit bietet Hellofresh seinen Kunden Gratisboxen an, die man an andere verschenken kann. Ich habe im Moment 3 Boxen zu verschenken. 

Der Gutschein gilt nur für Neukunden und für mindestens eine Classic Box mit 3 Gerichte für 2 Personen. Ob andere Boxen auch Gratix sind, müsste ausprobiert werden.
Man schliesst kein Abo ab, sondern kann nach jeder Woche ganz flexibel die Lieferung stoppen oder 1 oder mehrere Wochen aussetzen. 

Es ist  auf jedenfall eine gute Gelegenheit, die Gerichte auszuprobieren. Ich find diese immer sehr lecker und die Portionen sind meistens mehr als ausreichend für 2 Personen(Bei einer Box für 2).

Wer also ein Interesse an einer Gratisbox hat, der möge sich so schnell wie möglich in den Kommentaren melden. Die Boxen sind meistens schnell weg.

~~Alternativ habe ich auch einen 20€ Gutschein, damit spart man 50% auf eine Box. Auch hier gilt nur für Neukunden!~~


