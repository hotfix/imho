---
title: "[How-To] Update Pi-hole In Docker"
date: 2023-10-23T09:03:20+01:00
draft: false
author: Alexander
tags: ["pihole", "raspberry", "docker", "pi"]
categories: ["faq", "tools"]

---

Es ist normalerseile kein Hexenwerk [pihole zu aktualisieren](https://hotfix.gitlab.io/imho/2020/03/14/wie-kann-pihole-aktualisiert-werden/).

Hat man jedoch sein pihole als Docker Contrainer laufen,  ist es nicht mehr möglich einfach mit dem Befehl ein Update durchzuführen.

```bash
$ docker exec -it pihole pihole -up
Function not supported in Docker images
```

Es muss der gesamte Docker Contrainer ersetzt bzw. aktualisiert werden.
Um Pi-hole-Docker-Container zu aktualisieren, muss zunächst über den Docker die neueste Pi-hole-Version abgefragt werden und den Container neu bereitzustellen.

``` bash
 $ docker pull pihole/pihole:latest
latest: Pulling from pihole/pihole
85e50d2242ce: Pull complete
205b3bcb04a1: Pull complete
4f4fb700ef54: Pull complete
efc47020d282: Pull complete
3a438191e6f2: Pull complete
a090ee43303f: Pull complete
10002067986b: Pull complete
30034a05debf: Pull complete
d61aa1a24eb4: Pull complete
Digest: sha256:562766abc805d5005bb2d2aa5d4bbf2d9b347380b1ea4504fb59b2100500f672
Status: Downloaded newer image for pihole/pihole:latest
docker.io/pihole/pihole:latest
```

{{< notice note >}}
Es ist wichtig, die neueste Version zu holen, **BEVOR**  die bestehende Pihole-Instanz entfernt wird, wenn man nur eine einzige Pihole-Instanz hat. Ansonsten wird man nicht in der Lage sein, irgendwelche Hostnamen aufzulösen.
{{< /notice >}}


Im nächsten Schritt kann man den Contrainer stoppen und danach auch entfernen.  `pihole` ist bei mir der Name des Contrainers. Wenn man einen anderen namen hat, dann kann man diesen mit dem Befehl `$ docker ps` herausfinden.

``` bash
 $ docker stop pihole
pihole
$ docker rm pihole
pihole

```

Jetzt kann die bereits vorhanden Konfigdatei, in meinem Fall ist das `docker-compose.yml`, verwendet werden um einen Docker Contrainer für Pihole zu erstellen:

``` bash
$ docker-compose -f ./docker-compose.yml up -d
```


Zum Schluß prüfen wir noch die pihole Version, um sicher zu gehen, dass alles geklappt hat:

``` bash
$ docker exec -it pihole pihole -v
  Pi-hole version is v5.17.1 (Latest: v5.17.2)
  web version is v5.20.1 (Latest: null)
  FTL version is v5.23 (Latest: v5.23)
```


## Referenzen:

- [Upgrading, Persistence, and Customizations](https://github.com/pi-hole/docker-pi-hole#upgrading-persistence-and-customizations)